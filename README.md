# Java Vulnerable Lab
## Prerequisites
**Install docker**
(https://docs.docker.com/v17.09/engine/installation/)

## How to run
```bash
$ docker-compose up
```

The service will be available in https://localhost:9443/

## Support Documentation
https://www.dcc.fc.up.pt/~edrdo/aulas/qses/projects/01/ferramentas.html
